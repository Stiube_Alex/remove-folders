# Delete Folders with Specific Name

This script allows you to delete all folders (and the files inside of them) with a specific name in a specific path. Before the script starts deleting folders, it will prompt you to confirm that you want to proceed with the deletion.

---

## Usage

1. Open your terminal and navigate to the directory where the script is saved.
2. Make the script is executable by running the following command (if needed):

```bash
chmod +x delete_folders.sh
```

3. Run the script by entering the following command:

```bash
bash delete_folders.sh
```

4. The script will prompt you to enter the path from which you want to delete the folders. Enter the full path and press Enter.
5. The script will prompt you to enter the name of the folder you want to delete. Enter the name of the folder and press Enter.
6. The script will display the number of folders it has found with the specified name in the specified path.
7. The script will prompt you to confirm whether you want to proceed with the deletion. If you enter `y` and press Enter, the script will start deleting the folders.
8. Once the folders have been deleted, the script will display a message indicating that the folders have been deleted and the paths of the deleted folders will be written to a log file.

---

## Notes

This script is case-insensitive. It will delete folders with the specified name regardless of whether the name is in uppercase or lowercase.

This script will permanently delete the folders. There is no way to recover the deleted folders once they have been deleted.

This script will not delete any files (except the ones that are inside the selected folder). It will only delete folders with the specified name.

---

## <span style='color:red'>Warning</span>

This script permanently deletes all folders with the specified name in the specified directory. Use with caution.
