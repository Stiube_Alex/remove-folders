#!/bin/bash

printf "\n"
printf "\t\033[1;31mWARNING!\033[0m\n"
printf "\tThis script will permanently delete folders.\n"
printf "\tPress Ctrl+C to cancel or Enter to continue.\n"
printf "\tDon't use any of the next character \\' \" \` \n"
printf "\tThe current version of this script is\033[1;31m not entirely case sensitive\033[0m.\n"
printf "\n"

# Ensure that the user has entered a valid partition path
read -r -p "Enter the path from where you want to delete the folders: " starting_path
if [ ! -d "$starting_path" ]; then
  echo "Invalid path or path does not exist. Aborting."
  exit 1
fi

# Ensure that the user has entered a valid folder name
read -r -p "Enter the name of the folder you want to delete: " folder_name
if [ -z "$folder_name" ]; then
  echo "Invalid folder name. Aborting."
  exit 1
fi
printf "\n"

# Use the find command to locate all folders with the specified name in the partition
while IFS= read -r -d '' folder; do
  echo "Found folder: $folder"
  folders+=("$folder")
done < <(find "$starting_path" -type d -name "$folder_name" -print0)

# Check if any folders were found
if [ ${#folders[@]} -eq 0 ]; then
  echo "No folders found with name $folder_name in $starting_path."
  exit 0
fi

# Print the number of folders that will be deleted
printf "\nNumber of folders to delete: %d\n" "${#folders[@]}"

# Ask for confirmation before proceeding with the deletion
read -r -p "Are you sure you want to delete these folders? [y/n] " confirm
if [ "$confirm" != "y" ]; then
  echo "Aborting."
  exit 1
fi

# Delete the folders and write the paths of the deleted folders to a log file
log_file="$(date +%Y-%m-%d).log"
for folder in "${folders[@]}"; do
  printf "Deleting: %s\n" "$folder"
  if rm -rf "$folder"; then
    printf "Deleted: %s\n" "$folder" >>"$log_file"
  else
    printf "Failed to delete: %s\n" "$folder"
  fi
done

# Print a message indicating that the folders have been deleted
printf "\nFolders with name %s have been deleted from %s\n" "$folder_name" "$starting_path"
printf "Paths of deleted folders have been appended to %s\n" "$log_file"
